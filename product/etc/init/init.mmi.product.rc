# common initialization
on post-fs
    # LMK
    setprop ro.lmk.kill_heaviest_task true
    setprop ro.lmk.file_low_percentage 20
    setprop ro.lmk.file_high_percentage 85
    setprop ro.lmk.pgscan_limit 4000
    setprop ro.lmk.swap_free_low_percentage 20
    setprop ro.lmk.swap_util_max 75
    setprop ro.lmk.thrashing_limit 40
    setprop ro.lmk.thrashing_limit_decay 30
    setprop ro.lmk.thrashing_limit_critical 50
    setprop ro.lmk.threshold_decay 50
    setprop ro.lmk.psi_complete_stall_ms 120
    setprop ro.lmk.psi_partial_stall_ms 70
    setprop ro.lmk.downgrade_pressure 100
    setprop ro.lmk.filecache_min_kb -1
    setprop ro.lmk.kill_timeout_ms 100
    setprop persist.lmk.debug true
    #Reinit lmkd on MTK
    setprop lmkd.reinit 1
    #Dalvik configuration
    setprop dalvik.vm.heapstartsize 8m
    setprop dalvik.vm.heapgrowthlimit 192m
    setprop dalvik.vm.heapsize 256m
    setprop dalvik.vm.heaptargetutilization 0.75
    setprop dalvik.vm.heapminfree 1m
    setprop dalvik.vm.heapmaxfree 8m
    # App compactor
    setprop ro.config.compact_action_1 4
    setprop ro.config.compact_action_2 2
    setprop ro.config.compact_procstate_throttle 11,18
    # Zram
    setprop ro.vendor.zram.product_swapon true
    setprop persist.sys.zram_wb_size 1024M
    setprop ro.zram.mark_idle_delay_mins 60
    setprop ro.zram.first_wb_delay_mins 1440
    setprop ro.zram.periodic_wb_delay_hours 24
    write /sys/block/zram0/comp_algorithm lz4
    write /proc/sys/vm/swappiness 60
    setprop sys.sysctl.swappiness 60
    #Set default psi for frameowkr
    setprop ro.lowmemdetector.psi_low_stall_us 150000
    setprop ro.lowmemdetector.psi_medium_stall_us 250000
    setprop ro.lowmemdetector.psi_high_stall_us 350000
    setprop ro.config.compact_bootcompleted true
    # Promote recent UI procs
    setprop persist.sys.aitune_promote_recent_ui_procs true
    # enable moto cpu boost framework.
    setprop persist.sys.perf_fwk_enabled true
    setprop persist.sys.allow_aosp_hints true

on property:sys.boot_completed=1
    write /dev/cpuset/top-app/cpus 0-7
    write /dev/cpuset/foreground/cpus 1-7
    write /dev/cpuset/system-background/cpus 2-7
    write /dev/cpuset/background/cpus 4-7
    setprop dalvik.vm.dex2oat-threads 4
    setprop dalvik.vm.dex2oat-cpu-set 4,5,6,7
    write /sys/devices/system/cpu/cpu0/cpufreq/scaling_governor schedutil
    write /sys/devices/system/cpu/cpu4/cpufreq/scaling_governor schedutil

on early-init
    write /sys/devices/system/cpu/cpu0/cpufreq/scaling_governor performance
    write /sys/devices/system/cpu/cpu4/cpufreq/scaling_governor performance

on property:sys.boot_completed=1 && property:ro.vendor.zram.product_swapon=true
    trigger sys-boot-completed-set

on sys-boot-completed-set && property:persist.sys.zram_wb_enabled=false
    swapon_all /vendor/etc/fstab.mtk
on sys-boot-completed-set && property:persist.sys.zram_wb_enabled=true
    swapon_all /vendor/etc/fstab.mtk.zramwb

# Disable Zram writeback
on property:persist.sys.zram_wb_enabled=""
    setprop persist.sys.zram_wb_enabled false

# Enable Process pool for 6G~12G RAM
on property:ro.vendor.hw.ram=2GB && property:persist.device_config.runtime_native.usap_pool_enabled=""
    setprop persist.device_config.runtime_native.usap_pool_enabled false
on property:ro.vendor.hw.ram=3GB && property:persist.device_config.runtime_native.usap_pool_enabled=""
    setprop persist.device_config.runtime_native.usap_pool_enabled false
on property:ro.vendor.hw.ram=4GB && property:persist.device_config.runtime_native.usap_pool_enabled=""
    setprop persist.device_config.runtime_native.usap_pool_enabled false
on property:ro.vendor.hw.ram=6GB && property:persist.device_config.runtime_native.usap_pool_enabled=""
    setprop persist.device_config.runtime_native.usap_pool_enabled true
on property:ro.vendor.hw.ram=8GB && property:persist.device_config.runtime_native.usap_pool_enabled=""
    setprop persist.device_config.runtime_native.usap_pool_enabled true
on property:ro.vendor.hw.ram=12GB && property:persist.device_config.runtime_native.usap_pool_enabled=""
    setprop persist.device_config.runtime_native.usap_pool_enabled true

# Enable App compactor for 2~4G RAM
on property:ro.vendor.hw.ram=2GB
    setprop ro.config.use_compaction true
on property:ro.vendor.hw.ram=3GB
    setprop ro.config.use_compaction true
on property:ro.vendor.hw.ram=4GB
    setprop ro.config.use_compaction true
# Enable App compactor for 6~8G RAM
on property:ro.vendor.hw.ram=6GB
    setprop ro.config.use_compaction true
on property:ro.vendor.hw.ram=8GB
    setprop ro.config.use_compaction true

# Tune Swap readahead on ufs 3.1
# 0 when zram wb was disabled
# 4 when zram wb was enabled, it should be fine tuned on each ufs/emmc.
# Note: readahead will only be applied on writeback pages.
on property:sys.boot_completed=1 && property:persist.sys.zram_wb_enabled=false
    write /proc/sys/vm/page-cluster 0
on property:sys.boot_completed=1 && property:persist.sys.zram_wb_enabled=true
    write /proc/sys/vm/page-cluster 4

# Tune Max bg apps
on property:ro.vendor.hw.ram=2GB && property:persist.sys.zram_wb_enabled=false
    setprop ro.MAX_HIDDEN_APPS 16
on property:ro.vendor.hw.ram=3GB && property:persist.sys.zram_wb_enabled=false
    setprop ro.MAX_HIDDEN_APPS 20
on property:ro.vendor.hw.ram=4GB && property:persist.sys.zram_wb_enabled=false
    setprop ro.MAX_HIDDEN_APPS 28
on property:ro.vendor.hw.ram=12GB && property:persist.sys.zram_wb_enabled=false
    setprop ro.MAX_HIDDEN_APPS 60
on property:ro.vendor.hw.ram=6GB && property:persist.sys.zram_wb_enabled=true
    setprop ro.MAX_HIDDEN_APPS 48
on property:ro.vendor.hw.ram=8GB && property:persist.sys.zram_wb_enabled=true
    setprop ro.MAX_HIDDEN_APPS 60
on property:ro.vendor.hw.ram=12GB && property:persist.sys.zram_wb_enabled=true
    setprop ro.MAX_HIDDEN_APPS 80

on property:ro.product.cpu.abi=arm64-v8a
    setprop dalvik.vm.dex2oat64.enabled true


