#
# Copyright (C) 2024 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/lineage_ellis.mk

COMMON_LUNCH_CHOICES := \
    lineage_ellis-user \
    lineage_ellis-userdebug \
    lineage_ellis-eng
